<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use Illuminate\Http\Request;

class FeatureController extends Controller {
    public function index() {
        return Feature::all();
    }

    public function show($id) {
        return Feature::find($id);
    }
}
