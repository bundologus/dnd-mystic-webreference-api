<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Action extends Model {
    use HasFactory;

    protected $fillable = [
        'action_type',
        'uses_resource',
        'add_proficiency',
    ];

    protected $with = [
        'effect',
        'area',
        'range',
        'rolls',
        'resourceGains',
    ];

    public function rolls()
    {
        return $this->hasMany(ActionRoll::class)->withDefault();
     }

    public function effect() {
        return $this->hasMany(ActionEffect::class)->withDefault();
    }

    public function range() {
        return $this->hasOne(ActionRange::class)->withDefault();
    }

    public function area() {
        return $this->hasOne(ActionArea::class)->withDefault();
    }

    public function resourceGains() {
        return $this->hasOne(ActionResourceGain::class)->withDefault();
    }
}
