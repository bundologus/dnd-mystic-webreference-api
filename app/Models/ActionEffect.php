<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionEffect extends Model {
    use HasFactory;

    protected $fillable = [
        'action_id',
        'damage_id',
    ];

    protected $with = [
        "effect",
    ];

    public function effect() {
        return $this->hasOne(Damage::class);
    }
}
