<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionRange extends Model {
    use HasFactory;

    protected $fillable = [
        'action_id',
        'range_value',
        'range_unit',
    ];
}
