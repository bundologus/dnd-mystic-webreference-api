<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionResourceGain extends Model
{
    use HasFactory;

    protected $fillable = [
        'action_id',
        'resource_name',
        'fix_value',
        'die_type',
        'die_count',
    ];

    public function resource() {
        return $this->hasOne(Resource::class);
    }
}
