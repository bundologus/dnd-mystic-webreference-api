<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionRoll extends Model {
    use HasFactory;

    protected $fillable = [
        'action_id',
        'bonus_value',
        'modifier_ability',
        'is_proficient',
    ];
}
