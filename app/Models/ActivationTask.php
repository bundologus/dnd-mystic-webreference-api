<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivationTask extends Model {
    use HasFactory;

    protected $fillable = [
        'modifier_id',
        'equipment_id',
    ];
}
