<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Background extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
    ];

    public function specialFeature() {
        return $this->hasOne(BackgroundSpecialFeature::class);
    }

    public function suggestedCharacteristic() {
        return $this->hasMany(SuggestedCharacteristic::class);
    }

    public function features() {
        return $this->hasMany(Feature::class);
    }
}
