<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BackgroundSpecialFeature extends Model {
    use HasFactory;

    protected $fillable = [
        'background_id',
        'name',
        'description',
    ];

    public function background() {
        return $this->hasOne(Background::class);
    }
}
