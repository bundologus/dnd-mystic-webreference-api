<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoxedText extends Model {
    use HasFactory;

    protected $fillable = [
        'flavortext_chapter_id',
        'title',
        'content',
    ];

    public function flavortextChapter() {
        return $this->belongsTo(FlavortextChapter::class);
    }
}
