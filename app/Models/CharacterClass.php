<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharacterClass extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'character_class_id',
        'quickbuild',
        'hit_die',
    ];

    public function parentClass() {
        return $this->belongsTo(CharacterClass::class);
    }

    public function subclasses() {
        return $this->hasMany(CharacterClass::class);
    }

    public function classLevels() {
        return $this->hasMany(ClassLevel::class);
    }
    
    public function playerCharacters() {
        return $this->belongsToMany(PlayerCharacter::class);
    }
    
    public function equipmentOptions() {
        return $this->hasMany(EquipmentOption::class)->withDefault();
    }
    
    public function proficiencies() {
        return $this->belongsToMany(Proficiency::class)->withDefault();
    }

    public function proficiencyChoices() {
        return $this->hasMany(ProficiencyChoice::class)->withDefault();
    }
}
