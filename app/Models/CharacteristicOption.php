<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharacteristicOption extends Model {
    use HasFactory;

    protected $fillable = [
        'suggested_characteristic_id',
        'roll_number',
        'description',
    ];

    public function suggestedCharacteristic() {
        return $this->belongsTo(SuggestedCharacteristic::class);
    }
}
