<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassLevel extends Model {
    use HasFactory;

    protected $fillable = [
        'character_class_id',
        'subclass_id',
        'level',
        'has_asi',
    ];

    public function characterClass() {
        return $this->belongsTo(CharacterClass::class);
    }

    public function subclass() {
        return $this->belongsTo(Subclass::class)->withDefault();
    }

    public function features() {
        return $this->hasMany(Feature::class)->withDefault();
    }
}
