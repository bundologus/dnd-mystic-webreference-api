<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassLevelDie extends Model {
    use HasFactory;

    protected $fillable = [
        'class_level_id',
        'die_count',
        'die_type',
    ];

    public function classLevel() {
        return $this->belongsTo(ClassLevel::class);
    }
}
