<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassLevelFixedValue extends Model {
    use HasFactory;

    protected $fillable = [
        'class_level_id',
        'value',
        'value_type',
        'postfix',
    ];

    public function classLevel() {
        return $this->belongsTo(ClassLevel::class);
    }
}
