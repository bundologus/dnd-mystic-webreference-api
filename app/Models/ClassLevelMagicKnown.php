<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassLevelMagicKnown extends Model {
    use HasFactory;

    protected $fillable = [
        'class_level_id',
        'known_value',
        'magic_type',
    ];

    public function classLevel() {
        return $this->belongsTo(ClassLevel::class);
    }
}
