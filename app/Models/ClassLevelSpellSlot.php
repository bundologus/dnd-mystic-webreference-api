<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassLevelSpellSlot extends Model {
    use HasFactory;

    protected $fillable = [
        'class_level_id',
        'slot_count',
        'slot_level',
    ];

    public function classLevel() {
        return $this->belongsTo(ClassLevel::class);
    }
}
