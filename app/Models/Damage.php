<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Damage extends Model {
    use HasFactory;

    protected $fillable = [
        'fix_value',
        'die_count',
        'die_type',
        'damage_type',
        'save_ability',
        'half_on_save',
    ];
}
