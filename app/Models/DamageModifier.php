<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DamageModifier extends Model {
    use HasFactory;

    protected $fillable = [
        'modifier_id',
        'damage_id',
        'damage_modifier_type',
    ];

    protected $with = [
        'damage'
    ];

    public function damage() {
        return $this->hasOne(Damage::class);
    }
}
