<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisciplineArea extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_discipline_id',
        'effect_area_id',
    ];

    protected $with = [
        'area'
    ];

    public function area() {
        return $this->hasOne(EffectArea::class);
    }
}
