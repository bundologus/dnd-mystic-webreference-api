<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisciplineEffect extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_discipline_id',
        'effect_id',
    ];

    protected $with = [
        'effect'
    ];

    public function effect() {
        return $this->hasOne(Damage::class);
    }
}
