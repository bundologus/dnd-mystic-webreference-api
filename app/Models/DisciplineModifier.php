<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisciplineModifier extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_discipline_id',
        'modifier_id',
    ];

    protected $with = [
        'modifier'
    ];

    public function modifier() {
        return $this->hasOne(Modifier::class);
    }
}
