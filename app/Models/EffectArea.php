<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EffectArea extends Model {
    use HasFactory;

    protected $fillable = [
        'area_value',
        'area_unit',
        'area_shape',
    ];
}
