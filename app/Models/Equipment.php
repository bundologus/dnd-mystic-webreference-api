<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'weight',
        'cost',
        'rarity',
    ];

    protected $with = [
        'actions',
        'charges',
        'features',
        'weaponData',
    ];

    public function charges() {
        return $this->hasOne(EquipmentCharge::class)->withDefault();
    }

    public function features() {
        return $this->hasMany(Feature::class)->withDefault();
    }

    public function actions() {
        return $this->hasMany(EquipmentAction::class)->withDefault();
    }

    public function weaponData() {
        return $this->hasOne(Weapon::class)->withDefault();
    }

    public function toolProficiency() {
        return $this->hasOne(ToolProficiency::class)->withDefault();
    }
}
