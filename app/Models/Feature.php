<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'replaces_feature_id',
        'features',
        'cascade',
        'cascade',
        'has_choice',
        'choice_count',
        'is_subclass_choice',
        'is_feat',
        'max_uses',
    ];

    public function proficiencies() {
        return $this->belongsToMany(Proficiency::class)->withDefault();
    }

    public function modifiers() {
        return $this->belongsToMany(Modifier::class)->withDefault();
    }

    public function actions() {
        return $this->hasMany(FeatureAction::class)->withDefault();
    }

    public function resources() {
        return $this->hasMany(FeatureResource::class)->withDefault();
    }

    public function classFeature() {
        return $this->hasOne(ClassFeature::class)->withDefault();
    }

    public function racialFeature() {
        return $this->hasOne(RacialFeature::class)->withDefault();
    }

    public function backgroundFeature() {
        return $this->hasOne(BackgroundFeature::class)->withDefault();
    }

    public function spellcasting() {
        return $this->hasOne(Spellcasting::class)->withDefault();
    }

    public function psionics() {
        return $this->hasOne(Psionics::class)->withDefault();
    }
}
