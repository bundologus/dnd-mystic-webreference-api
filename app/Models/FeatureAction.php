<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function Symfony\Component\String\b;

class FeatureAction extends Model
{
    use HasFactory;

    protected $fillable = [
        "feature_id",
        "action_id",
    ];

    protected $with = [
        'action'
    ];

    public function action() {
        return $this->hasOne(Action::class);
    }
}
