<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeatureResource extends Model
{
    use HasFactory;

    protected $fillable = [
        'feature_id',
        'resource_name',
    ];
}
