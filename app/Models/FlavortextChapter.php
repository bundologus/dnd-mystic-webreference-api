<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlavortextChapter extends Model {
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
    ];

    public function boxedText() {
        return $this->hasMany(BoxedText::class)->withDefault();
    }
}
