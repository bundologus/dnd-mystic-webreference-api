<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImprovedPtArea extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_talent_improvement_id',
        'effect_area_id',
    ];

    protected $with = [
        'area',
    ];

    public function area() {
        return $this->hasOne(EffectArea::class);
    }
}
