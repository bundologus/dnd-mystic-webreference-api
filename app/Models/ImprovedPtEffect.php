<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImprovedPtEffect extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_talent_improvement_id',
        'effect_id',
    ];

    protected $with = [
        'effect',
    ];

    public function effect() {
        return $this->hasOne(Damage::class);
    }
}
