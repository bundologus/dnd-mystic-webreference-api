<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImprovedSpellDuration extends Model {
    use HasFactory;

    protected $fillable = [
        'spell_improvement_id',
        'duration_value',
    ];
}
