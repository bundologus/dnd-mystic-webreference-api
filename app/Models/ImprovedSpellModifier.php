<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImprovedSpellModifier extends Model {
    use HasFactory;

    protected $fillable = [
        'spell_improvement_id',
        'modifier_id',
    ];

    protected $with = [
        "modifier"
    ];

    public function modifier() {
        return $this->hasOne(Modifier::class);
    }
}
