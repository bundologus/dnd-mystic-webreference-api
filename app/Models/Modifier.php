<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modifier extends Model {
    use HasFactory;

    protected $fillable = [
        'is_active',
        'modifier_type',
        'subtype',
        'value',
        'is_debuff',
        'is_template',
        'is_override',
    ];

    public function proficiency() {
        return $this->hasOne(ProficiencyModifier::class)->withDefault();
    }

    public function damage() {
        return $this->hasOne(DamageModifier::class)->withDefault();
    }

    public function targetEquipment() {
        return $this->hasMany(ModifierTargetEquipment::class)->withDefault();
    }

    public function activationTask() {
        return $this->hasOne(ActivationTask::class)->withDefault();
    }
}
