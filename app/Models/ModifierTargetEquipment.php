<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModifierTargetEquipment extends Model {
    use HasFactory;

    protected $fillable = [
        'modifier_id',
        'equipment_id',
    ];

    protected $with = [
        'equipment'
    ];

    public function equipment() {
        return $this->belongsTo(equipment::class);
    }
}
