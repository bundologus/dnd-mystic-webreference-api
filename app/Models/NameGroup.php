<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NameGroup extends Model {
    use HasFactory;

    protected $fillable = [
        'race_id',
        'name',
        'name_list',
    ];

    public function race() {
        return $this->belongsTo(Race::class);
    }
}
