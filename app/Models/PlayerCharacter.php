<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerCharacter extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'background_id',
        'race_id',
        'strength_score',
        'dexterity_score',
        'constitution_score',
        'intelligence_score',
        'wisdom_score',
        'charisma_score',
        'size_category',
        'walking_speed',
        'flying_speed',
        'climbing_speed',
        'swimming_speed',
        'burrowing_speed',
        'darkvision_radius',
        'blindsight_radius',
        'true_sight_radius',
        'tremor_sense_radius',
    ];

    // TODO
    /*
    public function resources() {
        return $this->hasMany(PcResource::class)->withDefault();
    }

    public function actions() {
        return $this->hasMany(PcAction::class)->withDefault();
    }

    public function modifiers() {
        return $this->hasMany(PcModifier::class)->withDefault();
    }

    public function proficiencies() {
        return $this->hasMany(PcProficiency::class)->withDefault();
    }

    public function disciplines() {
        return $this->hasMany(PcDiscipline::class)->withDefault();
    }

    public function talents() {
        return $this->hasMany(PcTalent::class)->withDefault();
    }

    public function spell() {
        return $this->hasMany(PcSpell::class)->withDefault();
    }

    public function equipment() {
        return $this->hasMany(PcEquipment::class)->withDefault();
    }

    public function bgCharacteristics() {
        return $this->belongsToMany(SuggestedCharacteristic::class, 'pc_bg_characteristics')->withPivot('roll_number')->withDefault();
    }

    public function classes() {
        return $this->belongsToMany(CharacterClass::class, 'pc_character_class')->withPivot('subclass_id', 'pc_class_level', 'is_primary')->withDefault();
    }
    */
}
