<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proficiency extends Model {
    use HasFactory;

    protected $fillable = [
        'proficiency_type',
        'subtype',
    ];

    protected $with = [
        "skill",
        "language",
        "equipment",
    ];

    public function skill() {
        return $this->hasOneThrough(Skill::class, SkillProficiency::class)->withDefault();
    }

    public function language() {
        return $this->hasOneThrough(Language::class, LanguageProficiency::class)->withDefault();
    }

    public function equipment() {
        return $this->hasOneThrough(Equipment::class, SingleEquipmentProficiency::class)->withDefault();
    }

    public function feature() {
        return $this->belongsToMany(Feature::class)->withDefault();
    }
}
