<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProficiencyModifier extends Model {
    use HasFactory;

    protected $fillable = [
        'modifier_id',
        'proficiency_id',
        'proficiency_modifier_type',
    ];

    protected $with = [
        'proficiency'
    ];

    public function proficiency() {
        return $this->belongsTo(Proficiency::class);
    }
}
