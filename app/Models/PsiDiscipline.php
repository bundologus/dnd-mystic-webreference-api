<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiDiscipline extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'focus',
    ];

    protected $with  = [
        'uses'
    ];

    public function uses() {
        return $this->hasMany(PsiDisciplineUse::class);
    }

    public function psionics() {
        return $this->belongsToMany(Psionics::class);
    }
}
