<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiDisciplineUse extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_discipline_id',
        'name',
        'description',
        'pp_cost',
        'pp_cost_max',
        'pp_cost_step',
        'activation_time_value',
        'activation_time_unit',
        'duration_value',
        'duration_unit',
        'is_concentration',
        'range_value',
        'range_unit',
    ];

    protected $with = [
        'area',
        'effect',
        'modifier',
    ];

    public function area() {
        return $this->hasOne(DisciplineArea::class)->withDefault();
    }

    public function effect() {
        return $this->hasOne(DisciplineEffect::class)->withDefault();
    }

    public function modifier() {
        return $this->hasMany(DisciplineModifier::class)->withDefault();
    }
    
    public function discipline() {
        return $this->belongsTo(Discipline::class);
    }
}
