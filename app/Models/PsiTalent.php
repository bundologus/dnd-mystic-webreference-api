<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiTalent extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'improvement_description',
        'action_type',
        'duration_value',
        'duration_unit',
        'range_value',
        'range_unit',
    ];

    public function area() {
        return $this->hasOne(PsiTalentArea::class)->withDefault();
    }

    public function effect() {
        return $this->hasOne(PsiTalentEffect::class)->withDefault();
    }

    public function modifiers() {
        return $this->hasMany(PsiTalentModifier::class)->withDefault();
    }

    public function improvements() {
        return $this->hasMany(PsiTalentImprovement::class)->withDefault();
    } 
    
    public function psionics() {
        return $this->belongsToMany(Psionics::class);
    }
}
