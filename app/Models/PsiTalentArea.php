<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiTalentArea extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_talent_id',
        'effect_area_id',
    ];

    protected $with = [
        'area'
    ];

    public function area() {
        return $this->hasOne(EffectArea::class);
    }
}
