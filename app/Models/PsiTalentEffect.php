<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiTalentEffect extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_talent_id',
        'damage_id',
    ];

    protected $with = [
        'damage'
    ];

    public function damage() {
        return $this->hasOne(Damage::class);
    }
}
