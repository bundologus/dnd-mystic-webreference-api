<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsiTalentImprovement extends Model {
    use HasFactory;

    protected $fillable = [
        'psi_talent_id',
        'character_level',
    ];

    protected $with = [
        'area',
        'effect',
        'modifier',
    ];

    public function area() {
        return $this->hasOne(ImprovedPtArea::class)->withDefault();
    }

    public function effect() {
        return $this->hasOne(ImprovedPtEffect::class)->withDefault();
    }

    public function modifiers() {
        return $this->hasMany(ImprovedPtModifier::class)->withDefault();
    }

    public function talent() {
        return $this->belongsTo(PsiTalent::class);
    }
}
