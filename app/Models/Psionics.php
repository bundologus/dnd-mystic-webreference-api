<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Psionics extends Model {
    use HasFactory;

    protected $fillable = [
        'ability',
        'has_focus',
        'ability',
    ];

    public function feature() {
        return $this->hasOne(Feature::class);
    }

    public function talents() {
        return $this->belongsToMany(PsiTalent::class)->withDefault();
    }

    public function disciplines() {
        return $this->belongsToMany(PsiDiscipline::class)->withDefault();
    }
}
