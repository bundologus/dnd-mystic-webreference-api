<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Race extends Model {
    use HasFactory;

    protected $fillable = [
        'race_id',
        'name',
        'description',
        'race_category',
    ];

    public function playerCharacters() {
        return $this->belongsToMany(PlayerCharacter::class)->withDefault();
    }

    public function subraces() {
        return $this->hasMany(Race::class)->withDefault();
    }

    public function racialAbilityBonuses() {
        return $this->hasMany(RacialAbilityBonus::class);
    }
}
