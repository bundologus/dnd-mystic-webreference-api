<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkillProficiency extends Model {
    use HasFactory;

    protected $fillable = [
        'proficiency_id',
        'skill_id',
    ];
}
