<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spell extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'magic_school',
        'spell_level',
        'is_ritual',
        'casting_time_value',
        'casting_time_unit',
        'duration_value',
        'duration_unit',
        'is_concentration',
        'range_value',
        'range_unit',
        'is_spell_attack',
        'is_verbal',
        'is_somatic',
        'is_material',
        'material_component',
    ];

    protected $with = [
        'effects',
        'area',
        'improvements',
    ];

    public function spellcasting() {
        return $this->belongsToMany(Spellcasting::class);
    }

    public function modifier() {
        return $this->hasMany(SpellModifier::class)->withDefault();
    }

    public function effects() {
        return $this->hasMany(SpellEffect::class)->withDefatult();
    }

    public function area() {
        return $this->hasOne(SpellArea::class)->withDefault();
    }

    public function improvements() {
        return $this->hasMany(SpellImprovement::class)->withDefault();
    }
}
