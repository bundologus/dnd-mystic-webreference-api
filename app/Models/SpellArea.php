<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpellArea extends Model {
    use HasFactory;

    protected $fillable = [
        'spell_id',
        'effect_area_id',
    ];

    protected $with = [
        'effectArea'
    ];

    public function effectArea() {
        return $this->hasOne(EffectArea::class);
    }
}
