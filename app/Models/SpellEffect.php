<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpellEffect extends Model {
    use HasFactory;

    protected $fillable = [
        'spell_id',
        'damage_id',
    ];

    protected $with = [
        'effect'
    ];

    public function effect() {
        return $this->hasOne(Damage::class);
    }
}
