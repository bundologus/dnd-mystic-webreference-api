<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpellImprovement extends Model {
    use HasFactory;

    protected $fillable = [
        'spell_id',
        'on_character_level',
        'per_higher_slot',
    ];

    protected $with = [
        "duration",
        "effect",
        "modifiers",
    ];

    public function duration() {
        return $this->hasOne(ImprovedSpellDuration::class)->withDefault();
    }

    public function effect() {
        return $this->hasOne(ImprovedSpellEffect::class)->withDefault();
    }

    public function modifiers() {
        return $this->hasMany(ImprovedSpellModifier::class)->withDefault();
    }
}
