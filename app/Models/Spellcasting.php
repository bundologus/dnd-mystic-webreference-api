<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spellcasting extends Model {
    use HasFactory;

    protected $fillable = [
        'ability',
        'knows_all',
        'has_ritual_casting',
        'is_always_prepared',
        'focus_type',
    ];

    public function feature() {
        return $this->hasOne(Feature::class);
    }

    public function spells() {
        return $this->belongsToMany(Spell::class);
    }
}
