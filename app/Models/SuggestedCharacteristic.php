<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuggestedCharacteristic extends Model {
    use HasFactory;

    protected $fillable = [
        'background_id',
        'characteristic_type',
        'description',
        'die_type',
    ];

    public function background() {
        return $this->belongsTo(Background::class);
    }

    public function options() {
        return $this->hasMany(CharacteristicOption::class);
    }
}
