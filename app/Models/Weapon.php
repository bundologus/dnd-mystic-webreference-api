<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weapon extends Model {
    use HasFactory;

    protected $fillable = [
        'equipment_id',
        'attack_type',
        'reach',
        'range',
        'extended_range',
        'magic_bonus',
    ];

    public function equipment() {
        return $this->hasOne(Equipment::class);
    }

    protected $with = [
        'damage',
        'properties',
        'groups',
    ];
    
    public function damage() {
        return $this->hasMany(WeaponDamage::class);
    }

    public function properties() {
        return $this->hasMany(WeaponProperty::class)->withDefault();
    }

    public function groups() {
        return $this->hasMany(WeaponGroup::class);
    }
    
    public function proficiency() {
        return $this->hasOne(WeaponProficiency::class)->withDefault();
    }
}
