<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeaponGroup extends Model {
    use HasFactory;

    protected $fillable = [
        'weapon_id',
        'group_name',
    ];

    public function weapons() {
        return $this->hasManyThrough(Equipment::class, Weapon::class)->withDefault();
    }
}
