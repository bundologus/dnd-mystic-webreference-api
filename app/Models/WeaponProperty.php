<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeaponProperty extends Model {
    use HasFactory;

    protected $fillable = [
        'weapon_id',
        'property_type',
    ];

    public function weapons() {
        return $this->hasManyThrough(Equipment::class, Weapon::class)->withDefault();
    }
}
