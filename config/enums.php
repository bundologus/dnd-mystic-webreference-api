<?php

return [
    'ability' => [
        'strength',
        'dexterity',
        'constitution',
        'intelligence',
        'wisdom',
        'charisma',
    ],

    'prof_type' => [
        'saving_throw',
        'skill',
        'armor',
        'weapon',
        'tool',
    ],

    'focus_type' => [
        'none',
        'musical_instrument',
        'holy_symbol',
        'druidic_focus',
        'arcane_focus',
    ],

    'magic_school' => [
        'abjuration',
        'conjuration',
        'divination',
        'enchantment',
        'evocation',
        'illusion',
        'necromancy',
        'transmutation',
    ],

    'casting_time_unit' => [
        'action',
        'bonus_action',
        'reaction',
        'minute',
        'hour',
        'special',
    ],

    'spell_duration_unit' => [
        'instantaneous',
        'round',
        'hour',
        'minute',
        'day',
        'until_dispelled',
        'until_dispelled_or_triggered',
        'special',
    ],

    'spell_range_unit' => [
        'self',
        'touch',
        'feet',
        'sight',
        'special',
    ],

    'spell_area_unit' => [
        'feet',
        'mile',
    ],

    'spell_area_shape' => [
        'cilinder',
        'cone',
        'cube',
        'line',
        'sphere',
        'square',
    ],

    'spell_target_type' => [
        'self',
        'ally',
        'any',
        'enemy',
    ],

    'race_category' => [
        'any',
        'aberration',
        'beast',
        'celestial',
        'construct',
        'dragon',
        'elemental',
        'fey',
        'fiend',
        'giant',
        'humanoid',
        'monstrosity',
        'ooze',
        'plant',
        'undead',
    ],

    'condition' => [
        'blinded',
        'charmed',
        'deafened',
        'exhausted_1',
        'exhausted_2',
        'exhausted_3',
        'exhausted_4',
        'exhausted_5',
        'exhausted_6',
        'frightened',
        'grappled',
        'incapacitated',
        'invisible',
        'paralyzed',
        'pertified',
        'poisoned',
        'prone',
        'restrained',
        'stunned',
        'unconscious',
    ],

    'damage_type' => [
        'acid',
        'bludgeoning',
        'cold',
        'fire',
        'force',
        'lightning',
        'necrotic',
        'piercing',
        'poison',
        'psychic',
        'radiant',
        'slashing',
        'thunder',
    ],

    'size_category' => [
        'tiny',
        'small',
        'medium',
        'large',
        'huge',
        'gargantuan',
    ],
];
