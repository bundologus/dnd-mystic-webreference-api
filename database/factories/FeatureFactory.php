<?php

namespace Database\Factories;

use App\Models\Feature;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeatureFactory extends Factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Feature::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        // ! BROKEN 

        /* $newFeature = [
            'title_en' => ucwords(join(" ", $this->faker->words(rand(1, 3)))),
            'description_en' => $this->faker->paragraph(),
        ];

        if (rand(1, 7) != 3) {
            $newFeature['class_level_id'] = rand(1, 6);
        }

        if (rand(1, 5) == 2) {
            $newFeature['title_hu'] = ucwords(join(" ", $this->faker->words(rand(1, 3))));
        }

        if (rand(1, 7) == 3) {
            $newFeature['title_hu'] = ucwords(join(" ", $this->faker->words(rand(1, 3))));
        } */

        return $newFeature = [];
    }
}
