<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_levels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('character_class_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('subclass_id')->constrained('character_class')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('level');
            $table->boolean('has_asi')->default(false);
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_levels');
    }
}
