<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelSpellSlotsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_level_spell_slots', function (Blueprint $table) {
            $table->id();
            $table->foreignId('class_level_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('slot_count');
            $table->integer('slot_level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_level_spell_slots');
    }
}
