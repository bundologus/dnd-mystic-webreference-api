<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelMagicKnownsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_level_magic_knowns', function (Blueprint $table) {
            $table->id();
            $table->foreignId('class_level_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('known_value');
            $table->string('magic_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_level_magic_knowns');
    }
}
