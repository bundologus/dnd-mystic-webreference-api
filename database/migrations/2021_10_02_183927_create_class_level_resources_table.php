<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelResourcesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_level_resources', function (Blueprint $table) {
            $table->id();
            $table->foreignId('class_level_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('resource_value');
            $table->string('resource_type');
            $table->string('postfix')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_level_resources');
    }
}
