<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelDiesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_level_dies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('class_level_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('die_count');
            $table->integer('die_type');
            $table->string('class_die_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_level_dies');
    }
}
