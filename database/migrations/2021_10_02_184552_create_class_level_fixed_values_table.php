<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassLevelFixedValuesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('class_level_fixed_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('class_level_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('value');
            $table->string('value_type');
            $table->string('postfix');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('class_level_fixed_values');
    }
}
