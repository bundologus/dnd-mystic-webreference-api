<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuggestedCharacteristicsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('suggested_characteristics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('background_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('characteristic_type');
            $table->text('description');
            $table->integer('die_type');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('suggested_characteristics');
    }
}
