<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacteristicOptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('characteristic_options', function (Blueprint $table) {
            $table->foreignId('suggested_characteristic_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->integer('roll_number');
            $table->text('description');
            $table->timestamps();

            $table->primary(['suggested_characteristic_id', 'roll_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('characteristic_options');
    }
}
