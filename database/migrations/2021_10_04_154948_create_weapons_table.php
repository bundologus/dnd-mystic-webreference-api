<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeaponsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('weapons', function (Blueprint $table) {
            $table->id();
            $table->foreignId('equipment_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('attack_type');
            $table->integer('reach');
            $table->integer('range');
            $table->integer('extended_range');
            $table->integer('magic_bonus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('weapons');
    }
}
