<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDamagesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('damages', function (Blueprint $table) {
            $table->id();
            $table->integer("fix_value");
            $table->integer("die_count");
            $table->integer("die_type");
            $table->string("damage_type");
            $table->string("save_ability")->nullable();
            $table->boolean("half_on_save");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('damages');
    }
}
