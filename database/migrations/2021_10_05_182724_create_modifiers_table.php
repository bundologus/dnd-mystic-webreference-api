<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModifiersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('modifiers', function (Blueprint $table) {
            $table->id();
            $table->boolean("is_active");
            $table->string("modifier_type");
            $table->string("subtype");
            $table->integer("value");
            $table->boolean("is_debuff");
            $table->boolean("is_template");
            $table->boolean("is_override");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('modifiers');
    }
}
