<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDamageModifiersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('damage_modifiers', function (Blueprint $table) {
            $table->foreignId("modifier_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("damage_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("damage_modifier_type");
            $table->timestamps();

            $table->primary(["modifier_id", "damage_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('damage_modifiers');
    }
}
