<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProficiencyModifiersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proficiency_modifiers', function (Blueprint $table) {
            $table->foreignId("modifier_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("proficiency_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("proficiency_modifier_type");
            $table->timestamps();

            $table->primary(["modifier_id", "proficiency_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proficiency_modifiers');
    }
}
