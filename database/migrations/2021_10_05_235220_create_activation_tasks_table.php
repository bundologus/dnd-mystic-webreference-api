<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationTasksTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('activation_tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId("modifier_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("task_type");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('activation_tasks');
    }
}
