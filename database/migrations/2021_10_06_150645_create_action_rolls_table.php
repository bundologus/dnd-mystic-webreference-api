<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionRollsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('action_rolls', function (Blueprint $table) {
            $table->id();
            $table->foreignId("action_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("bonus_value");
            $table->string("modifier_ability");
            $table->boolean("is_proficient");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('action_rolls');
    }
}
