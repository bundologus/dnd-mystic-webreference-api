<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionRangesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('action_ranges', function (Blueprint $table) {
            $table->id();
            $table->foreignId("action_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("range_value");
            $table->string("range_unit");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('action_ranges');
    }
}
