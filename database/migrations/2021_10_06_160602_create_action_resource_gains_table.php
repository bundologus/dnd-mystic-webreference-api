<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionResourceGainsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('action_resource_gains', function (Blueprint $table) {
            $table->foreignId("action_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("resource_name");
            $table->integer("fix_value");
            $table->integer("die_type");
            $table->integer("die_count");
            $table->timestamps();

            $table->foreign("resource_name")->references("name")->on('resources');
            $table->primary(["action_id", "resource_name"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('action_resource_gains');
    }
}
