<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpellsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('spells', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("description");
            $table->text("improvement_description")->nullable();
            $table->string("magic_school");
            $table->integer("spell_level");
            $table->boolean("is_ritual");
            $table->integer("casting_time_value");
            $table->string("casting_time_unit");
            $table->integer("duration_value");
            $table->string("duration_unit");
            $table->boolean("is_concentration");
            $table->integer("range_value");
            $table->string("range_unit");
            $table->boolean("is_spell_attack");
            $table->boolean("is_verbal");
            $table->boolean("is_somatic");
            $table->boolean("is_material");
            $table->string("material_component");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('spells');
    }
}
