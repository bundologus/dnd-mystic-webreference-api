<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpellImprovementsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('spell_improvements', function (Blueprint $table) {
            $table->id();
            $table->foreignId("spell_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("on_character_level");
            $table->boolean("per_higher_slot");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('spell_improvements');
    }
}
