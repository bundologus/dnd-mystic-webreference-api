<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImprovedSpellDurationsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('improved_spell_durations', function (Blueprint $table) {
            $table->id();
            $table->foreignId("spell_improvement_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("duration_value");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('improved_spell_durations');
    }
}
