<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImprovedSpellEffectsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('improved_spell_effects', function (Blueprint $table) {
            $table->foreignId("spell_improvement_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("damage_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();

            $table->primary(["spell_improvement_id", "damage_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('improved_spell_effects');
    }
}
