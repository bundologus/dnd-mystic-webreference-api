<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsiTalentTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('psi_talent', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("description");
            $table->text("improvement_description");
            $table->string("action_type");
            $table->integer("duration_value");
            $table->string("duration_unit");
            $table->integer("range_value");
            $table->string("range_unit");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('psi_talent');
    }
}
