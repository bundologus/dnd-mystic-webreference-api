<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsiTalentEffectsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('psi_talent_effects', function (Blueprint $table) {
            $table->foreignId("psi_talent_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("damage_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();

            $table->primary(["psi_talent_id", "damage_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('psi_talent_effects');
    }
}
