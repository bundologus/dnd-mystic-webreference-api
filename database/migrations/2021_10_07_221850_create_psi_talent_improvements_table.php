<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsiTalentImprovementsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('psi_talent_improvements', function (Blueprint $table) {
            $table->id();
            $table->foreignId("psi_talent_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("character_level");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('psi_talent_improvements');
    }
}
