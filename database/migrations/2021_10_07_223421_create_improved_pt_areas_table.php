<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImprovedPtAreasTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('improved_pt_areas', function (Blueprint $table) {
            $table->foreignId("psi_talent_improvement_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("effect_area_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();

            $table->primary(["psi_talent_improvement_id", "effect_area_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('improved_pt_areas');
    }
}
