<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePsiDisciplineUsesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('psi_discipline_uses', function (Blueprint $table) {
            $table->id();
            $table->foreignId("psi_discipline_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("name");
            $table->text("description");
            $table->integer("pp_cost");
            $table->integer("pp_cost_max");
            $table->integer("pp_cost_step");
            $table->integer("activation_time_value");
            $table->string("activation_time_unit");
            $table->integer("duration_value");
            $table->string("duration_unit");
            $table->boolean("is_concentration");
            $table->integer("range_value");
            $table->string("range_unit");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('psi_discipline_uses');
    }
}
