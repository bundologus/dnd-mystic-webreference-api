<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisciplineAreasTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('discipline_areas', function (Blueprint $table) {
            $table->foreignId("psi_discipline_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("effect_area_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();

            $table->primary(["psi_discipline_id", "effect_area_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('discipline_areas');
    }
}
