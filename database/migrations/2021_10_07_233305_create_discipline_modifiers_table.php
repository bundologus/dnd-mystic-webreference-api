<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisciplineModifiersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('discipline_modifiers', function (Blueprint $table) {
            $table->foreignId("psi_discipline_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("modifier_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();

            $table->primary(["psi_discipline_id", "modifier_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('discipline_modifiers');
    }
}
