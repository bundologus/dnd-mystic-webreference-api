<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->foreignId('replaces_feature_id')
                ->nullable()
                ->constrained('features')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('has_choice')->default(false);
            $table->integer('choice_count')->default(1);
            $table->boolean('is_subclass_choice')->default(false);
            $table->boolean('is_feat')->default(false);
            $table->integer('max_uses');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('features');
    }
}
