<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_resources', function (Blueprint $table) {
            $table->foreignId("feature_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("resource_name");
            $table->timestamps();

            $table->foreign('resource_name')->references('name')->on('resources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_resources');
    }
}
