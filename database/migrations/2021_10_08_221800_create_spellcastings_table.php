<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpellcastingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('spellcastings', function (Blueprint $table) {
            $table->id();
            $table->string('ability');
            $table->boolean('knows_all');
            $table->boolean('has_ritual_casting');
            $table->boolean('is_always_prepared');
            $table->string('focus_type');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('spellcastings');
    }
}
