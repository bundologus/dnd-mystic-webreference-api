<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_characters', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->string("name");
            $table->foreignId("background_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("race_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("strength_score");
            $table->integer("dexterity_score");
            $table->integer("constitution_score");
            $table->integer("intelligence_score");
            $table->integer("wisdom_score");
            $table->integer("charisma_score");
            $table->string("size_category");
            $table->integer("walking_speed");
            $table->integer("flying_speed");
            $table->integer("climbing_speed");
            $table->integer("swimming_speed");
            $table->integer("burrowing_speed");
            $table->integer("darkvision_radius");
            $table->integer("blindsight_radius");
            $table->integer("true_sight_radius");
            $table->integer("tremor_sense_radius");
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_characters');
    }
}
