<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePcBgCharacteristicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pc_bg_characteristics', function (Blueprint $table) {
            $table->foreignId("suggested_characteristic_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("player_character_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->integer("roll_number");

            $table->primary(["suggested_characteristic_id", "player_character_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pc_bg_characteristics');
    }
}
