<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePcCharacterClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pc_character_classes', function (Blueprint $table) {
            $table->foreignId("character_class_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("player_character_id")->constrained()->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("subclass_id")->constrained("character_classes")->onUpdate("cascade")->onDelete("cascade");
            $table->integer("pc_class_level");
            $table->boolean("is_primary");

            $table->primary(["character_class_id", "player_character_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pc_character_classes');
    }
}
