<?php

namespace Database\Seeders;

use App\Models\CharacterClass;
use App\Models\ClassLevel;
use Illuminate\Database\Seeder;

class CharacterClassSeeder extends Seeder {
    /**
     * Seeder for all tables in the Class group:
     *  - CharacterClass
     *  - ClassLevel
     *  - ClassLevelMagicKnown
     *  - ClassLevelSpellSlot
     *  - ClassLevelResource
     *  - ClassLevelDie
     *  - ClassLevelFixedValue
     *  - ClassFlavortext
     *  - ClassFeature
     * 
     * By default, it contains seed information for the Mystic class form an Unearthed Arcana publication.
     * @return void
     */
    public function run() {

        // * Unearthed Arcana - The Mistic Class

        CharacterClass::insert([
            'name' => 'Mystic',
            'description' => '',
            'quickbuld' => '',
            'hit_die' => 8,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Avatar',
            'description' => 'Mystics of the Order of the Avatar delve into the world of emotion, mastering their inner life to such an extent that they can manipulate and amplify the emotions of others with the same ease that an artist shapes clay. Known as Avatars, these mystics vary from tyrants to inspiring leaders who are loved by their followers.<br>Avatars can bring out extreme emotions in the people around them. For their allies, they can lend hope, ferocity, and courage, transforming a fighting band into a deadly, unified force. For their enemies, they bring fear, disgust, and 
            trepidation that can make even the most hardened veteran act like a shaky rookie.',
            'character_class_id' => 1,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Awakened',
            'description' => 'Mystics dedicated to the Order of the Awakened seek to unlock the full potential of the mind. By transcending the physical, the Awakened hope to attain a state of being focused on pure intellect and mental energy.<br>The Awakened are skilled at bending minds and unleashing devastating psionic attacks, and they can read the secrets of the world through psionic energy. Awakened mystics who take to adventuring excel at unraveling mysteries, solving puzzles, and defeating monsters by turning them into unwilling pawns.',
            'character_class_id' => 1,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Immortal',
            'description' => 'The Order of the Immortal uses psionic energy to augment and modify the physical form. Followers of this order are known as Immortals. They use psionic energy to modify their bodies, strengthening them against attack and turning themselves into living weapons.<br>Their mastery of the physical form grants them their name, for Immortals are notoriously difficult to kill.',
            'character_class_id' => 1,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Nomad',
            'description' => 'Mystics of the Order of the Nomad keep their minds in a strange, rarified state. They seek to accumulate as much knowledge as possible, as they quest to unravel the mysteries of the multiverse and seek the underlying structure of all things. At the same time, they perceive a bizarre, living web of knowledge they call the noosphere.<br>Nomads, as their name indicates, delight in travel, exploration, and discovery. They desire to accumulate as much knowledge as possible, and the pursuit of secrets and hidden lore can become an obsession for them.',
            'character_class_id' => 1,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Soul Knife',
            'description' => 'The Order of the Soul Knife sacrifices the breadth of knowledge other mystics gain to focus on a specific psionic technique. These mystics learn to manifest a deadly weapon of pure psychic energy that they can use to cleave through foes.<brySoul knives vary widely in their approach to this path. Some follow it out of a desire to achieve martial perfection. Others are ruthless assassins who seek to become the perfect killer.',
            'character_class_id' => 1,
        ]);

        CharacterClass::insert([
            'name' => 'Order of the Wu Jen',
            'description' => 'The Order of the Wu Jen features some of the most devoted mystics. These mystics seek to lock themselves away from the world, denying the limits of the physical world and replacing it with a reality that they create for themselves. Known as wu jens, these mystics cast their minds into the world, seize control of its fundamental principles, and rebuild it.<br>In practical terms, wu jens excel at controlling the forces of the natural world. They can hurl objects with their minds, control the four elements, and alter reality to fit their desires.',
            'character_class_id' => 1,
        ]);


        $mistic_id = CharacterClass::where('name', 'Mystic')->first();
        $immortal_id = CharacterClass::where('name', 'Order of the Immortal')->first();

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'level' => 1,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'subclass_id' => $immortal_id,
            'level' => 1,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'subclass_id' => $immortal_id,
            'level' => 2,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'level' => 3,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'subclass_id' => $immortal_id,
            'level' => 3,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'level' => 4,
            'has_asi' => true,
        ]);

        ClassLevel::insert([
            'character_class_id' => $mistic_id,
            'level' => 5,
        ]);
    }
}
