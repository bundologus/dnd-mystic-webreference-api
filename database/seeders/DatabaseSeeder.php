<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'admin',
            'email' => 'admin@szerveroldali.hu',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            'is_admin' => true,
            'remember_token' => Str::random(10),
        ]);
        $this->call(GlobalClassSeeder::class);
        $this->call(GlobalFeatureSeeder::class);
    }
}
