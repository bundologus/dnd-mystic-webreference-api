<?php

namespace Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // * MYSTIC CLASS 1-5

        Feature::insert([
            'name_en' => 'Mystic Order',
            'name_hu' => 'Misztikus Tanok',
            'description_en' => '',
            'class_level_id' => 1,
            'is_subclass_choice' => true,
        ]);

        Feature::insert([
            'name_en' => 'Mystical Recovery',
            'name_hu' => 'Misztikus Felfrissülés',
            'description_en' => '',
            'class_level_id' => 2,
        ]);

        Feature::insert([
            'name_en' => 'Telepathy',
            'name_hu' => 'Telepátia',
            'description_en' => '',
            'class_level_id' => 2,
        ]);

        Feature::insert([
            'name_en' => 'Strength of Mind',
            'name_hu' => 'Az Elme Ereje',
            'description_en' => '',
            'class_level_id' => 4,
        ]);


        // * ORDER OF THE IMMORTAL 1-5

        Feature::insert([
            'name_en' => 'Bonus Disciplines',
            'name_hu' => 'Bónusz Technikák',
            'description_en' => '',
            'class_level_id' => 1,
            'subclass_id' => 4,
        ]);

        Feature::insert([
            'name_en' => 'Immortal Durability',
            'name_hu' => 'Sérthetetlen Test',
            'description_en' => '',
            'class_level_id' => 1,
            'subclass_id' => 4,
        ]);

        Feature::insert([
            'name_en' => 'Psionic Resilience',
            'name_hu' => 'Pszionikus Szívósság',
            'description_en' => '',
            'class_level_id' => 3,
            'subclass_id' => 4,
        ]);
    }
}
