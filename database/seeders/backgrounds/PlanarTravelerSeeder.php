<?php

namespace Database\Seeders\Backgrounds;

use App\Models\Background;
use App\Models\BackgroundSpecialFeature;
use App\Models\CharacteristicOption;
use App\Models\SuggestedCharacteristic;
use Illuminate\Database\Seeder;

class PlanarTravelerSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Background::insert([
            'name' => 'Síkközi utazó',
            'description' => '',
        ]);

        $planarTraveler_id = Background::where('name', 'Síkközi utazó')->first();

        BackgroundSpecialFeature::insert([
            'background_id' => $planarTraveler_id,
            'name' => 'Mindenhol Otthon',
            'description' => 'Annyi különböző helyen jártál már, annyi mindent tapasztaltál, hogy egyáltalán nem jelent gondot egy új kultúra magadba szívása. Néhány óra felfedezésre van csupán szükséged, hogy kiismerd magad egy környéken, magadra öltsd a helyi akcentust (amennyiben ismered a nyelvet), és el tudsz vegyülni.'
        ]);

        SuggestedCharacteristic::insert([
            'background_id' => $planarTraveler_id,
            'characteristic_type' => 'personality_trait',
            'description' => 'Lorem ipsum',
            'die_type' => 8,
        ]);

        $personalityTrait_id = SuggestedCharacteristic::where('background_id', $planarTraveler_id)->where('characteristic_type', 'personality_trait')->first();

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 1,
            'description' => '1. opció',
        ]);

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 2,
            'description' => '2. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 3,
            'description' => '3. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 4,
            'description' => '4. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 5,
            'description' => '5. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 6,
            'description' => '6. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 7,
            'description' => '7. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $personalityTrait_id,
            'roll_number' => 8,
            'description' => '8. opció',
        ]);



        SuggestedCharacteristic::insert([
            'background_id' => $planarTraveler_id,
            'characteristic_type' => 'ideal',
            'description' => 'Lorem ipsum',
            'die_type' => 6,
        ]);

        $ideal_id = SuggestedCharacteristic::where('background_id', $planarTraveler_id)->where('characteristic_type', 'ideal')->first();

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 1,
            'description' => '1. opció',
        ]);

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 2,
            'description' => '2. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 3,
            'description' => '3. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 4,
            'description' => '4. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 5,
            'description' => '5. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $ideal_id,
            'roll_number' => 6,
            'description' => '6. opció',
        ]);



        SuggestedCharacteristic::insert([
            'background_id' => $planarTraveler_id,
            'characteristic_type' => 'bond',
            'description' => 'Lorem ipsum',
            'die_type' => 6,
        ]);

        $bond_id = SuggestedCharacteristic::where('background_id', $planarTraveler_id)->where('characteristic_type', 'bond')->first();

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 1,
            'description' => '1. opció',
        ]);

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 2,
            'description' => '2. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 3,
            'description' => '3. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 4,
            'description' => '4. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 5,
            'description' => '5. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $bond_id,
            'roll_number' => 6,
            'description' => '6. opció',
        ]);



        SuggestedCharacteristic::insert([
            'background_id' => $planarTraveler_id,
            'characteristic_type' => 'flaw',
            'description' => 'Lorem ipsum',
            'die_type' => 6,
        ]);

        $flaw_id = SuggestedCharacteristic::where('background_id', $planarTraveler_id)->where('characteristic_type', 'flaw')->first();

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 1,
            'description' => '1. opció',
        ]);

        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 2,
            'description' => '2. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 3,
            'description' => '3. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 4,
            'description' => '4. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 5,
            'description' => '5. opció',
        ]);
        
        CharacteristicOption::insert([
            'suggested_characteristic_id' => $flaw_id,
            'roll_number' => 6,
            'description' => '6. opció',
        ]);
    }
}