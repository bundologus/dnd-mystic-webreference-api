<?php

namespace Database\Seeders;

use App\Models\Race;
use Illuminate\Database\Seeder;

class RaceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Race::insert([
            'name' => 'Genasi',
            'description' => '',
        ]);

        Race::insert([
            'race_id' => 'fire genasi',
            'name' => 'tűz genasi',
            'description' => '',
        ]);
    }
}
