<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'A megadott felhasználónév jelszó páros nem szerepel az adatbázisban.',
    'password' => 'A megadott jelszó helytelen.',
    'throttle' => 'Túl sokszor próbáltál bejelentkezni. Kérlek próbáld újra :seconds másodperc múlva.',

];
